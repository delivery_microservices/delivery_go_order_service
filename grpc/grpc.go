package grpc

import (
	"gitlab.com/delivery_microservices/delivery_go_order_service/config"
	"gitlab.com/delivery_microservices/delivery_go_order_service/genproto/order_service"
	"gitlab.com/delivery_microservices/delivery_go_order_service/grpc/client"
	"gitlab.com/delivery_microservices/delivery_go_order_service/grpc/service"
	"gitlab.com/delivery_microservices/delivery_go_order_service/pkg/logger"
	"gitlab.com/delivery_microservices/delivery_go_order_service/storage"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {
	grpcServer = grpc.NewServer()

	order_service.RegisterOrderServiceServer(grpcServer, service.NewOrderService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)

	return
}
