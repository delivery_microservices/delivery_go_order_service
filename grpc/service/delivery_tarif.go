package service

import (
	"context"

	"gitlab.com/delivery_microservices/delivery_go_order_service/config"
	"gitlab.com/delivery_microservices/delivery_go_order_service/genproto/order_service"
	"gitlab.com/delivery_microservices/delivery_go_order_service/grpc/client"
	"gitlab.com/delivery_microservices/delivery_go_order_service/pkg/logger"
	"gitlab.com/delivery_microservices/delivery_go_order_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type DeliveryTarifService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*order_service.UnimplementedDeliveryTarifServiceServer
}

func NewDeliveryTarifService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *DeliveryTarifService {
	return &DeliveryTarifService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (u *DeliveryTarifService) Create(ctx context.Context, req *order_service.TarifCreateReq) (*order_service.TarifCreateResp, error) {
	u.log.Info("====== DeliveryTarif Create ======", logger.Any("req", req))

	resp, err := u.strg.DeliveryTarif().Create(ctx, req)
	if err != nil {
		u.log.Error("error while creating delivery tarif", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *DeliveryTarifService) GetList(ctx context.Context, req *order_service.TarifGetListReq) (*order_service.TarifGetListResp, error) {
	u.log.Info("====== DeliveryTarif GetList ======", logger.Any("req", req))

	resp, err := u.strg.DeliveryTarif().GetList(ctx, req)
	if err != nil {
		u.log.Error("error while getlist delivery tarif", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *DeliveryTarifService) GetById(ctx context.Context, req *order_service.TarifIdReq) (*order_service.DeliveryTarif, error) {
	u.log.Info("====== DeliveryTarif GetById ======", logger.Any("req", req))

	resp, err := u.strg.DeliveryTarif().GetById(ctx, req)
	if err != nil {
		u.log.Error("error while get delivery tarif", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *DeliveryTarifService) Update(ctx context.Context, req *order_service.TarifUpdateReq) (*order_service.TarifUpdateResp, error) {
	u.log.Info("====== DeliveryTarif Update ======", logger.Any("req", req))

	resp, err := u.strg.DeliveryTarif().Update(ctx, req)
	if err != nil {
		u.log.Error("error while update delivery tarif", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *DeliveryTarifService) Delete(ctx context.Context, req *order_service.TarifIdReq) (*order_service.TarifDeleteResp, error) {
	u.log.Info("====== DeliveryTarif Delete ======", logger.Any("req", req))

	resp, err := u.strg.DeliveryTarif().Delete(ctx, req)
	if err != nil {
		u.log.Error("error while delete delivery tarif", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}
