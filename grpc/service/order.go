package service

import (
	"context"

	"gitlab.com/delivery_microservices/delivery_go_order_service/config"
	"gitlab.com/delivery_microservices/delivery_go_order_service/genproto/order_service"
	"gitlab.com/delivery_microservices/delivery_go_order_service/grpc/client"
	"gitlab.com/delivery_microservices/delivery_go_order_service/pkg/logger"
	"gitlab.com/delivery_microservices/delivery_go_order_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type OrderService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*order_service.UnimplementedOrderServiceServer
}

func NewOrderService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *OrderService {
	return &OrderService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (u *OrderService) Create(ctx context.Context, req *order_service.OrderCreateReq) (*order_service.OrderCreateResp, error) {
	u.log.Info("====== Order Create ======", logger.Any("req", req))

	resp, err := u.strg.Order().Create(ctx, req)
	if err != nil {
		u.log.Error("error while creating order", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *OrderService) GetList(ctx context.Context, req *order_service.OrderGetListReq) (*order_service.OrderGetListResp, error) {
	u.log.Info("====== Order GetList ======", logger.Any("req", req))

	resp, err := u.strg.Order().GetList(ctx, req)
	if err != nil {
		u.log.Error("error while get list order", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *OrderService) GetById(ctx context.Context, req *order_service.OrderIdReq) (*order_service.Order, error) {
	u.log.Info("====== Order GetById ======", logger.Any("req", req))

	resp, err := u.strg.Order().GetById(ctx, req)
	if err != nil {
		u.log.Error("error while get order", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *OrderService) Update(ctx context.Context, req *order_service.OrderUpdateReq) (*order_service.OrderUpdateResp, error) {
	u.log.Info("====== Order Update ======", logger.Any("req", req))

	resp, err := u.strg.Order().Update(ctx, req)
	if err != nil {
		u.log.Error("error while update order", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *OrderService) Delete(ctx context.Context, req *order_service.OrderIdReq) (*order_service.OrderDeleteResp, error) {
	u.log.Info("====== Order Delete ======", logger.Any("req", req))

	resp, err := u.strg.Order().Delete(ctx, req)
	if err != nil {
		u.log.Error("error while delete order", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *OrderService) ChangeStatus(ctx context.Context, req *order_service.OrderChangeStatusReq) (*order_service.OrderChangeStatusResp, error) {
	u.log.Info("====== Order ChangeStatus ======", logger.Any("req", req))

	resp, err := u.strg.Order().ChangeStatus(ctx, req)
	if err != nil {
		u.log.Error("error while change status order", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}
