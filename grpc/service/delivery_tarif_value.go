package service

import (
	"context"

	"gitlab.com/delivery_microservices/delivery_go_order_service/config"
	"gitlab.com/delivery_microservices/delivery_go_order_service/genproto/order_service"
	"gitlab.com/delivery_microservices/delivery_go_order_service/grpc/client"
	"gitlab.com/delivery_microservices/delivery_go_order_service/pkg/logger"
	"gitlab.com/delivery_microservices/delivery_go_order_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type DeliveryTarifValueService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*order_service.UnimplementedDeliveryTarifValueServiceServer
}

func NewDeliveryTarifValueService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *DeliveryTarifValueService {
	return &DeliveryTarifValueService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (u *DeliveryTarifValueService) Create(ctx context.Context, req *order_service.DeliveryTarifValue) (*order_service.TarifValueCreateResp, error) {
	u.log.Info("====== DeliveryTarifValue Create ======", logger.Any("req", req))

	resp, err := u.strg.DeliveryTarifValue().Create(ctx, req)
	if err != nil {
		u.log.Error("error while creating delivery tarif value", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *DeliveryTarifValueService) GetList(ctx context.Context, req *order_service.TarifValueGetListReq) (*order_service.TarifValueGetListResp, error) {
	u.log.Info("====== DeliveryTarifValue GetList ======", logger.Any("req", req))

	resp, err := u.strg.DeliveryTarifValue().GetList(ctx, req)
	if err != nil {
		u.log.Error("error while getlist delivery tarif value", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}
