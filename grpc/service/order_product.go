package service

import (
	"context"

	"gitlab.com/delivery_microservices/delivery_go_order_service/config"
	"gitlab.com/delivery_microservices/delivery_go_order_service/genproto/order_service"
	"gitlab.com/delivery_microservices/delivery_go_order_service/grpc/client"
	"gitlab.com/delivery_microservices/delivery_go_order_service/pkg/logger"
	"gitlab.com/delivery_microservices/delivery_go_order_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type OrderProductService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*order_service.UnimplementedOrderProductServiceServer
}

func NewOrderProductService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *OrderProductService {
	return &OrderProductService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (u *OrderProductService) Create(ctx context.Context, req *order_service.OrderProductCreateReq) (*order_service.OrderProductCreateResp, error) {
	u.log.Info("====== OrderProduct Create ======", logger.Any("req", req))

	resp, err := u.strg.OrderProduct().Create(ctx, req)
	if err != nil {
		u.log.Error("error while creating order product", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *OrderProductService) GetList(ctx context.Context, req *order_service.OrderProductGetListReq) (*order_service.OrderProductGetListResp, error) {
	u.log.Info("====== OrderProduct GetList ======", logger.Any("req", req))

	resp, err := u.strg.OrderProduct().GetList(ctx, req)
	if err != nil {
		u.log.Error("error while get list order product", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *OrderProductService) GetById(ctx context.Context, req *order_service.OrderProductIdReq) (*order_service.OrderProduct, error) {
	u.log.Info("====== OrderProduct GetById ======", logger.Any("req", req))

	resp, err := u.strg.OrderProduct().GetById(ctx, req)
	if err != nil {
		u.log.Error("error while get order product", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *OrderProductService) Update(ctx context.Context, req *order_service.OrderProductUpdateReq) (*order_service.OrderProductUpdateResp, error) {
	u.log.Info("====== OrderProduct Update ======", logger.Any("req", req))

	resp, err := u.strg.OrderProduct().Update(ctx, req)
	if err != nil {
		u.log.Error("error while update order product", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *OrderProductService) Delete(ctx context.Context, req *order_service.OrderProductIdReq) (*order_service.OrderDeleteResp, error) {
	u.log.Info("====== OrderProduct Delete ======", logger.Any("req", req))

	resp, err := u.strg.OrderProduct().Delete(ctx, req)
	if err != nil {
		u.log.Error("error while delete order product", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}
