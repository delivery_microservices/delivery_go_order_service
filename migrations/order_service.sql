CREATE TYPE "order_type" AS ENUM (
  'delivery',
  'pick_up'
);

CREATE TYPE "order_status" AS ENUM (
  'accepted',
  'courier_accepted',
  'ready_in_branch',
  'on_way',
  'finished',
  'canceled'
);

CREATE TYPE "order_payment_type" AS ENUM (
  'cash',
  'card'
);

CREATE TYPE "delivery_tarif_type" AS ENUM (
  'fixed',
  'alternative'
);

CREATE TABLE "orders" (
  "id" serial PRIMARY KEY,
  "order_id" varchar(20) NOT NULL,
  "client_id" int NOT NULL,
  "branch_id" int NOT NULL,
  "type" order_type NOT NULL,
  "address" text,
  "courier_id" int,
  "price" numeric NOT NULL,
  "delivery_price" numeric,
  "discount" numeric,
  "status" order_status,
  "payment_type" order_payment_type,
  "created_at" timestamp DEFAULT (now()),
  "updated_at" timestamp,
  "deleted_at" timestamp
);

CREATE TABLE "order_products" (
  "order_id" int,
  "product_id" int,
  "quantity" int,
  "price" numeric
);

CREATE TABLE "delivery_tarif" (
  "id" serial PRIMARY KEY,
  "name" varchar(255),
  "type" delivery_tarif_type,
  "base_price" numeric
);

CREATE TABLE "delivery_tarif_values" (
  "delivery_tarif_id" int,
  "from_price" numeric,
  "to_price" numeric,
  "price" numeric
);

ALTER TABLE "delivery_tarif_values" ADD FOREIGN KEY ("delivery_tarif_id") REFERENCES "delivery_tarif" ("id");

ALTER TABLE "order_products" ADD FOREIGN KEY ("order_id") REFERENCES "orders" ("id");

CREATE OR REPLACE FUNCTION generate_order_id() RETURNS TRIGGER AS $$
DECLARE
  next_id text;
BEGIN
  -- Find the next available ID by querying the maximum existing ID
  SELECT lpad(CAST(COALESCE(MAX(order_id::integer), 0) + 1 AS text), 6, '0') INTO next_id FROM "orders";
  
  NEW.order_id := next_id;
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;

-- Create a trigger that calls the above function before INSERT on the "orders" table
CREATE TRIGGER generate_order_id_trigger
BEFORE INSERT ON "orders"
FOR EACH ROW
EXECUTE FUNCTION generate_order_id();

