package postgres

import (
	"context"
	"fmt"

	"github.com/jackc/pgx/v5/pgxpool"
	"gitlab.com/delivery_microservices/delivery_go_order_service/config"
	"gitlab.com/delivery_microservices/delivery_go_order_service/storage"
)

type Store struct {
	db                  *pgxpool.Pool
	orders              *OrderRepo
	orderProducts       *OrderProductRepo
	deliveryTarifs      *DeliveryTarifRepo
	deliveryTarifValues *DeliveryTarifValueRepo
}

func NewPostgres(ctx context.Context, cfg config.Config) (storage.StorageI, error) {
	config, err := pgxpool.ParseConfig(
		fmt.Sprintf("postgres://%s:%s@%s:%d/%s?sslmode=disable",
			cfg.PostgresUser,
			cfg.PostgresPassword,
			cfg.PostgresHost,
			cfg.PostgresPort,
			cfg.PostgresDatabase,
		),
	)
	if err != nil {
		return nil, err
	}

	config.MaxConns = cfg.PostgresMaxConnections
	pool, err := pgxpool.NewWithConfig(ctx, config)
	if err != nil {
		return nil, err
	}

	return &Store{
		db: pool,
	}, nil
}

func (s *Store) CloseDB() {
	s.db.Close()
}

func (s *Store) Order() storage.OrderRepoI {
	if s.orders == nil {
		s.orders = NewOrderRepo(s.db)
	}

	return s.orders
}

func (s *Store) OrderProduct() storage.OrderProductRepoI {
	if s.orderProducts == nil {
		s.orderProducts = NewOrderProductRepo(s.db)
	}

	return s.orderProducts
}

func (s *Store) DeliveryTarif() storage.DeliveryTarifRepoI {
	if s.deliveryTarifs == nil {
		s.deliveryTarifs = NewDeliveryTarifRepo(s.db)
	}

	return s.deliveryTarifs
}

func (s *Store) DeliveryTarifValue() storage.DeliveryTarifValueRepoI {
	if s.deliveryTarifValues == nil {
		s.deliveryTarifValues = NewDeliveryTarifValueRepo(s.db)
	}

	return s.deliveryTarifValues
}
