package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/jackc/pgx"
	"github.com/jackc/pgx/v5/pgxpool"
	"gitlab.com/delivery_microservices/delivery_go_order_service/genproto/order_service"
)

type DeliveryTarifRepo struct {
	db *pgxpool.Pool
}

func NewDeliveryTarifRepo(db *pgxpool.Pool) *DeliveryTarifRepo {
	return &DeliveryTarifRepo{
		db: db,
	}
}

func (r *DeliveryTarifRepo) Create(ctx context.Context, req *order_service.TarifCreateReq) (*order_service.TarifCreateResp, error) {
	var id int
	query := `
	INSERT INTO delivery_tarifs (
		name,
		type,
		base_price
	) VALUES ($1,$2,$3)
	RETURNING id;`

	if err := r.db.QueryRow(ctx, query,
		req.Name,
		req.Typ,
		req.BasePrice,
	).Scan(&id); err != nil {
		return nil, err
	}

	return &order_service.TarifCreateResp{
		Msg: fmt.Sprintf("%d", id),
	}, nil
}

func (r *DeliveryTarifRepo) GetList(ctx context.Context, req *order_service.TarifGetListReq) (*order_service.TarifGetListResp, error) {
	var (
		filter    = " WHERE deleted_at IS NULL "
		offsetQ   = " OFFSET 0;"
		limit     = " LIMIT 10 "
		offset    = (req.Page - 1) * req.Limit
		count     int
		updatedAt sql.NullString
	)

	s := `
	SELECT 
		id,
		name,
		type,
		base_price,
		created_at::TEXT,
		updated_at::TEXT
	FROM order_products `

	if req.Name != "" {
		filter += " AND name ILIKE '%" + req.Name + "%' "
	}
	if req.Typ != "" {
		filter += " AND type = '" + req.Typ + "' "
	}
	if req.Limit > 0 {
		limit = fmt.Sprintf("LIMIT %d", req.Limit)
	}
	if offset > 0 {
		offsetQ = fmt.Sprintf("OFFSET %d", offset)
	}

	query := s + filter + limit + offsetQ

	countS := `SELECT COUNT(*) FROM delivery_tarifs` + filter

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	err = r.db.QueryRow(ctx, countS).Scan(&count)
	if err != nil {
		return nil, err
	}

	resp := &order_service.TarifGetListResp{}
	for rows.Next() {
		var tarif = order_service.DeliveryTarif{}
		if err := rows.Scan(
			&tarif.Id,
			&tarif.Name,
			&tarif.Typ,
			&tarif.BasePrice,
			&tarif.CreatedAt,
			&updatedAt,
		); err != nil {
			return nil, err
		}

		tarif.UpdatedAt = updatedAt.String

		resp.Tarifs = append(resp.Tarifs, &tarif)
		resp.Count = int64(count)
	}

	return resp, nil
}

func (r *DeliveryTarifRepo) GetById(ctx context.Context, req *order_service.TarifIdReq) (*order_service.DeliveryTarif, error) {
	var (
		updatedAt sql.NullString
	)
	query := `
	SELECT 
		id,
		name,
		type,
		base_price,
		created_at::TEXT,
		updated_at::TEXT
	FROM delivery_tarifs
	WHERE id = $1;`

	var tarif = order_service.DeliveryTarif{}
	if err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&tarif.Id,
		&tarif.Name,
		&tarif.Typ,
		&tarif.BasePrice,
		&tarif.CreatedAt,
		&updatedAt,
	); err != nil {
		return nil, err
	}

	tarif.UpdatedAt = updatedAt.String

	return &tarif, nil
}

func (r DeliveryTarifRepo) Update(ctx context.Context, req *order_service.TarifUpdateReq) (*order_service.TarifUpdateResp, error) {
	query := `
	UPDATE delivery_tarifs
	SET 
		name=$2
		type=$3,
		base_price=$4
	WHERE id = $1;`

	resp, err := r.db.Exec(ctx, query,
		req.Id,
		req.Name,
		req.Typ,
		req.BasePrice,
	)

	if err != nil {
		return nil, err
	}

	if resp.RowsAffected() == 0 {
		return nil, pgx.ErrNoRows
	}

	return &order_service.TarifUpdateResp{Msg: "success"}, nil
}

func (r *DeliveryTarifRepo) Delete(ctx context.Context, req *order_service.TarifIdReq) (*order_service.TarifDeleteResp, error) {
	query := `
    UPDATE delivery_tarifs 
    SET 
        deleted_at=NOW() 
    WHERE id=$1;`

	res, err := r.db.Exec(ctx, query,
		req.Id,
	)

	if err != nil {
		return nil, err
	}
	if res.RowsAffected() == 0 {
		return nil, pgx.ErrNoRows
	}

	return &order_service.TarifDeleteResp{Msg: "success"}, nil
}
