package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/jackc/pgx"
	"github.com/jackc/pgx/v5/pgxpool"
	"gitlab.com/delivery_microservices/delivery_go_order_service/genproto/order_service"
)

type OrderRepo struct {
	db *pgxpool.Pool
}

func NewOrderRepo(db *pgxpool.Pool) *OrderRepo {
	return &OrderRepo{
		db: db,
	}
}

func (r *OrderRepo) Create(ctx context.Context, req *order_service.OrderCreateReq) (*order_service.OrderCreateResp, error) {
	var id int
	query := `
	INSERT INTO orders (
		client_id,
		branch_id,
		type,
		address,
		courier_id,
		price,
		delivery_price,
		discount,
		status,
		payment_type
	)
	VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10)
	RETURNING id;`

	if err := r.db.QueryRow(ctx, query,
		req.ClientId,
		req.BranchId,
		req.Typ,
		req.Address,
		req.CourierId,
		req.Price,
		req.DeliveryPrice,
		req.Discount,
		req.Status,
		req.PaymentType,
	).Scan(&id); err != nil {
		return nil, err
	}

	return &order_service.OrderCreateResp{
		Msg: fmt.Sprintf("%d", id),
	}, nil
}

func (r *OrderRepo) GetList(ctx context.Context, req *order_service.OrderGetListReq) (*order_service.OrderGetListResp, error) {
	var (
		filter        = " WHERE deleted_at IS NULL "
		offsetQ       = " OFFSET 0;"
		limit         = " LIMIT 10 "
		offset        = (req.Page - 1) * req.Limit
		count         int
		updatedAt     sql.NullString
		deliveryPrice sql.NullFloat64
		discount      sql.NullFloat64
		status        sql.NullString
		paymentType   sql.NullString
	)

	s := `
	SELECT 
		id,
		order_id,
		client_id,
		branch_id,
		type,
		address,
		courier_id,
		price,
		delivery_price,
		discount,
		status,
		payment_type,
		created_at::TEXT,
		updated_at::TEXT 
	FROM orders `

	if req.OrderId != "" {
		filter += ` AND order_id='` + req.OrderId + "' "
	}
	if req.ClientId != "" {
		filter += ` AND client_id = ` + req.ClientId + " "
	}
	if req.BranchId != "" {
		filter += ` AND branch_id = ` + req.BranchId + " "
	}
	if req.Typ != "" {
		filter += ` AND type = '` + req.Typ + `' `
	}
	if req.CourierId != "" {
		filter += ` AND courier_id = ` + req.CourierId + " "
	}
	if req.PriceFrom != "" {
		filter += ` AND price > ` + req.PriceFrom + " "
	}
	if req.PriceTo != "" {
		filter += ` AND price < ` + req.PriceTo + " "
	}
	if req.PaymentType != "" {
		filter += ` AND payment_type = '` + req.PaymentType + `' `
	}
	if req.Limit > 0 {
		limit = fmt.Sprintf("LIMIT %d", req.Limit)
	}
	if offset > 0 {
		offsetQ = fmt.Sprintf("OFFSET %d", offset)
	}

	query := s + filter + limit + offsetQ

	countS := `SELECT COUNT(*) FROM orders` + filter

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	err = r.db.QueryRow(ctx, countS).Scan(&count)
	if err != nil {
		return nil, err
	}

	resp := &order_service.OrderGetListResp{}
	for rows.Next() {
		var order = order_service.Order{}
		if err := rows.Scan(
			&order.Id,
			&order.OrderId,
			&order.ClientId,
			&order.BranchId,
			&order.Typ,
			&order.Address,
			&order.CourierId,
			&order.Price,
			&deliveryPrice,
			&discount,
			&status,
			&paymentType,
			&order.CreatedAt,
			&updatedAt,
		); err != nil {
			return nil, err
		}

		order.DeliveryPrice = float32(deliveryPrice.Float64)
		order.Discount = float32(discount.Float64)
		order.Status = status.String
		order.PaymentType = paymentType.String
		order.UpdatedAt = updatedAt.String

		resp.Orders = append(resp.Orders, &order)
		resp.Count = int64(count)
	}

	return resp, nil
}

func (r *OrderRepo) GetById(ctx context.Context, req *order_service.OrderIdReq) (*order_service.Order, error) {
	var (
		updatedAt     sql.NullString
		deliveryPrice sql.NullFloat64
		discount      sql.NullFloat64
		status        sql.NullString
		paymentType   sql.NullString
	)
	query := `
	SELECT 
		id,
		order_id,
		client_id,
		branch_id,
		type,
		address,
		courier_id,
		price,
		delivery_price,
		discount,
		status,
		payment_type,
		created_at::TEXT,
		updated_at::TEXT 
	FROM orders
	WHERE id = $1;`

	var order = order_service.Order{}
	if err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&order.Id,
		&order.OrderId,
		&order.ClientId,
		&order.BranchId,
		&order.Typ,
		&order.Address,
		&order.CourierId,
		&order.Price,
		&deliveryPrice,
		&discount,
		&status,
		&paymentType,
		&order.CreatedAt,
		&updatedAt,
	); err != nil {
		return nil, err
	}

	order.DeliveryPrice = float32(deliveryPrice.Float64)
	order.Discount = float32(discount.Float64)
	order.Status = status.String
	order.PaymentType = paymentType.String
	order.UpdatedAt = updatedAt.String

	return &order, nil
}

func (r *OrderRepo) Update(ctx context.Context, req *order_service.OrderUpdateReq) (*order_service.OrderUpdateResp, error) {
	query := `
	UPDATE orders
	SET 
		client_id=$2,
		branch_id=$3,
		type=$4,
		address=$5,
		courier_id=$6,
		price=$7,
		delivery_price=$8,
		discount=$9,
		status=$10,
		payment_type=$11
	WHERE id = $1;`

	resp, err := r.db.Exec(ctx, query,
		req.Id,
		req.ClientId,
		req.BranchId,
		req.Typ,
		req.Address,
		req.CourierId,
		req.Price,
		req.DeliveryPrice,
		req.Discount,
		req.Status,
		req.PaymentType,
	)

	if err != nil {
		return nil, err
	}

	if resp.RowsAffected() == 0 {
		return nil, pgx.ErrNoRows
	}

	return &order_service.OrderUpdateResp{Msg: "success"}, nil
}

func (r *OrderRepo) Delete(ctx context.Context, req *order_service.OrderIdReq) (*order_service.OrderDeleteResp, error) {
	query := `
	UPDATE orders
	SET 
		deleted_at=NOW()
	WHERE id=$1;`

	res, err := r.db.Exec(ctx, query,
		req.Id,
	)

	if err != nil {
		return nil, err
	}
	if res.RowsAffected() == 0 {
		return nil, pgx.ErrNoRows
	}

	return &order_service.OrderDeleteResp{Msg: "success"}, nil
}

func (r *OrderRepo) ChangeStatus(ctx context.Context, req *order_service.OrderChangeStatusReq) (*order_service.OrderChangeStatusResp, error) {
	query := `
	UPDATE orders
	SET
		status=$2
	WHERE id=$1;`

	resp, err := r.db.Exec(ctx, query, req.Id, req.Status)

	if err != nil {
		return nil, err
	}

	if resp.RowsAffected() == 0 {
		return nil, pgx.ErrNoRows
	}

	return &order_service.OrderChangeStatusResp{Msg: "success"}, nil
}
