package postgres

import (
	"context"
	"fmt"

	"github.com/jackc/pgx/v5/pgxpool"
	"gitlab.com/delivery_microservices/delivery_go_order_service/genproto/order_service"
)

type DeliveryTarifValueRepo struct {
	db *pgxpool.Pool
}

func NewDeliveryTarifValueRepo(db *pgxpool.Pool) *DeliveryTarifValueRepo {
	return &DeliveryTarifValueRepo{
		db: db,
	}
}

func (r *DeliveryTarifValueRepo) Create(ctx context.Context, req *order_service.DeliveryTarifValue) (*order_service.TarifValueCreateResp, error) {
	query := `
	INSERT INTO delivery_tarif_values(
		delivery_tarif_id,
		from_price,
		to_price,
		price
	) VALUES($1,$2,$3,$4);`

	_, err := r.db.Exec(ctx, query,
		req.DeliveryTarifId,
		req.FromPrice,
		req.ToPrice,
		req.Price,
	)

	if err != nil {
		return nil, err
	}

	return &order_service.TarifValueCreateResp{Msg: "success"}, nil
}

func (r *DeliveryTarifValueRepo) GetList(ctx context.Context, req *order_service.TarifValueGetListReq) (*order_service.TarifValueGetListResp, error) {
	var (
		filter  = " WHERE deleted_at IS NULL "
		offsetQ = " OFFSET 0;"
		limit   = " LIMIT 10 "
		offset  = (req.Page - 1) * req.Limit
		count   int
	)

	s := `
	SELECT 
		delivery_tarif_id,
		from_price,
		to_price,
		price
	FROM delivery_tarif_values `

	if req.DeliveryTarifId != "" {
		filter += " AND delivery_tarif_id = '" + req.DeliveryTarifId + "' "
	}
	if req.Limit > 0 {
		limit = fmt.Sprintf("LIMIT %d", req.Limit)
	}
	if offset > 0 {
		offsetQ = fmt.Sprintf("OFFSET %d", offset)
	}

	query := s + filter + limit + offsetQ

	countS := `SELECT COUNT(*) FROM delivery_tarif_values` + filter

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	err = r.db.QueryRow(ctx, countS).Scan(&count)
	if err != nil {
		return nil, err
	}

	resp := &order_service.TarifValueGetListResp{}
	for rows.Next() {
		var tarifValue = order_service.DeliveryTarifValue{}
		if err := rows.Scan(
			&tarifValue.DeliveryTarifId,
			&tarifValue.FromPrice,
			&tarifValue.ToPrice,
			&tarifValue.Price,
		); err != nil {
			return nil, err
		}

		resp.TarifValues = append(resp.TarifValues, &tarifValue)
		resp.Count = int64(count)
	}

	return resp, nil
}
