package storage

import (
	"context"

	"gitlab.com/delivery_microservices/delivery_go_order_service/genproto/order_service"
)

type StorageI interface {
	CloseDB()
	Order() OrderRepoI
	OrderProduct() OrderProductRepoI
	DeliveryTarif() DeliveryTarifRepoI
	DeliveryTarifValue() DeliveryTarifValueRepoI
}

type OrderRepoI interface {
	Create(ctx context.Context, req *order_service.OrderCreateReq) (*order_service.OrderCreateResp, error)
	GetList(ctx context.Context, req *order_service.OrderGetListReq) (*order_service.OrderGetListResp, error)
	GetById(ctx context.Context, req *order_service.OrderIdReq) (*order_service.Order, error)
	Update(ctx context.Context, req *order_service.OrderUpdateReq) (*order_service.OrderUpdateResp, error)
	Delete(ctx context.Context, req *order_service.OrderIdReq) (*order_service.OrderDeleteResp, error)
	ChangeStatus(ctx context.Context, req *order_service.OrderChangeStatusReq) (*order_service.OrderChangeStatusResp, error)
}

type OrderProductRepoI interface {
	Create(ctx context.Context, req *order_service.OrderProductCreateReq) (*order_service.OrderProductCreateResp, error)
	GetList(ctx context.Context, req *order_service.OrderProductGetListReq) (*order_service.OrderProductGetListResp, error)
	GetById(ctx context.Context, req *order_service.OrderProductIdReq) (*order_service.OrderProduct, error)
	Update(ctx context.Context, req *order_service.OrderProductUpdateReq) (*order_service.OrderProductUpdateResp, error)
	Delete(ctx context.Context, req *order_service.OrderProductIdReq) (*order_service.OrderDeleteResp, error)
}

type DeliveryTarifRepoI interface {
	Create(ctx context.Context, req *order_service.TarifCreateReq) (*order_service.TarifCreateResp, error)
	GetList(ctx context.Context, req *order_service.TarifGetListReq) (*order_service.TarifGetListResp, error)
	GetById(ctx context.Context, req *order_service.TarifIdReq) (*order_service.DeliveryTarif, error)
	Update(ctx context.Context, req *order_service.TarifUpdateReq) (*order_service.TarifUpdateResp, error)
	Delete(ctx context.Context, req *order_service.TarifIdReq) (*order_service.TarifDeleteResp, error)
}

type DeliveryTarifValueRepoI interface {
	Create(ctx context.Context, req *order_service.DeliveryTarifValue) (*order_service.TarifValueCreateResp, error)
	GetList(ctx context.Context, req *order_service.TarifValueGetListReq) (*order_service.TarifValueGetListResp, error)
}
